FROM openjdk:8-jre-slim

EXPOSE 8080

RUN mkdir /app

COPY build/libs/*.jar /app/spring-boot-application.jar
ADD src/main/resources/application.yml /app/application.yml

CMD java -jar /app/spring-boot-application.jar --spring.config.location=/app/application.yml
