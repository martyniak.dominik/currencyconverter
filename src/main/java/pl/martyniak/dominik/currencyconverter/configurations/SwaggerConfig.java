package pl.martyniak.dominik.currencyconverter.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.HashSet;

/**
 * Defines configuration of the SwaggerAPI.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * Config Bean of swagger
     *
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.basePackage("pl.martyniak.dominik"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(new ApiInfoBuilder()
                        .title("CurrencyConverter")
                        .description("REST interface to CurrencyConverter")
                        .build()
                )
                .protocols(new HashSet<>(Collections.singletonList("http")));
    }
}
