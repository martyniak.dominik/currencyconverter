package pl.martyniak.dominik.currencyconverter.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.martyniak.dominik.currencyconverter.models.dto.ExchangeRateDTO;

import java.util.LinkedList;

/**
 * FeignClient to NBP Client
 */
@FeignClient(name = "NBPApiClient", url = "#{'${nbp-api-config.url}'}")
public interface NBPClient {

    /**
     * Method to get table with Rate data
     * @return - List of {@link ExchangeRateDTO}
     */
    @GetMapping(value = "/exchangerates/tables/{tableType}")
    LinkedList<ExchangeRateDTO> getActualExchangeRates(@PathVariable(value = "tableType") String tableType);


}
