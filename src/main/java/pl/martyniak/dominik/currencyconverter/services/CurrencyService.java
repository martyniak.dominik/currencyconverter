package pl.martyniak.dominik.currencyconverter.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import pl.martyniak.dominik.currencyconverter.databse.RatesRegistry;
import pl.martyniak.dominik.currencyconverter.models.dto.ConvertResponseDTO;
import pl.martyniak.dominik.currencyconverter.models.dto.ExchangeRateDTO;
import pl.martyniak.dominik.currencyconverter.models.dto.RateDTO;
import pl.martyniak.dominik.currencyconverter.utils.IncorrectAmountException;
import pl.martyniak.dominik.currencyconverter.utils.NotFoundCurrencyCodeException;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Service to handle Currency Logic
 *
 */
@Service
@RequiredArgsConstructor
@Log4j2
public class CurrencyService {

    private final RatesRegistry ratesRegistry;


    /**
     * Method return all saved Currency Code
     *
     * @return List<String>
     */
    public List<String> getAvailableCurrencyCode() {
        return ratesRegistry.getAll()
                .stream()
                .map(RateDTO::getCode)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Method to convert given amount with input Current to output currency
     * taking into account the value of the value
     * @param amount the value to be converted
     * @param inputCurrency code of input currency
     * @param outputCurrency code of output currency
     * @return ConvertResponseDTO  {@link ConvertResponseDTO}
     */
    public ConvertResponseDTO convertAmount(BigDecimal amount, String inputCurrency, String outputCurrency) {

        verifyAmount(amount);
        RateDTO inputRate = ratesRegistry.getByCode(inputCurrency);
        RateDTO outputRate = ratesRegistry.getByCode(outputCurrency);

        if (inputRate == null) {
            notFoundCurrency(inputCurrency);
        }
        if (outputRate == null) {
            notFoundCurrency(outputCurrency);
        }
        BigDecimal askValue = inputRate.getAsk().multiply(amount, MathContext.DECIMAL128);
        BigDecimal buyValue = askValue.divide(outputRate.getBid(),MathContext.DECIMAL128);

        return ConvertResponseDTO
                .builder()
                .amount(amount)
                .convertedAmount(buyValue)
                .inputCurrency(inputCurrency)
                .outputCurrency(outputCurrency).build();

    }

    private void verifyAmount(BigDecimal amount){
        if(amount.compareTo(BigDecimal.ZERO) <=0){
            log.warn(String.format("Incorrect Amount Value: %s", amount));
            throw new IncorrectAmountException(String.format("Incorrect Amount Value: %s", amount));
        }

    }


    private void notFoundCurrency(String currencyCode) {
        log.warn(String.format("Not found code %s, please use api/currency/available-currency-code to get list off available-currency-code", currencyCode));
        throw new NotFoundCurrencyCodeException(String.format("Not found code %s, please use api/currency/available-currency-code to get list off available-currency-code", currencyCode));
    }
}
