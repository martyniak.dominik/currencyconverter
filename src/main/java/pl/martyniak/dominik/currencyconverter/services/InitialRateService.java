package pl.martyniak.dominik.currencyconverter.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import pl.martyniak.dominik.currencyconverter.clients.NBPClient;
import pl.martyniak.dominik.currencyconverter.databse.RatesRegistry;
import pl.martyniak.dominik.currencyconverter.models.dto.ExchangeRateDTO;
import pl.martyniak.dominik.currencyconverter.models.dto.RateDTO;

import java.math.BigDecimal;
import java.util.LinkedList;


/**
 * Service that is controlled after starting the application
 */
@Service
@RequiredArgsConstructor
@Log4j2
public class InitialRateService {

    private final NBPClient nbpClient;
    private final RatesRegistry ratesRegistry;

    private final String TABLE_TYPE = "C";


    /**
     * The Methods is run after that the application is ready to service requests
     * The current exchange rates of table C are downloaded here from NBP API
     */
    @EventListener(ApplicationReadyEvent.class)
    public void startClient() {
        log.info("Try get all Rates");
        LinkedList<ExchangeRateDTO> exchangeRateDTO = nbpClient.getActualExchangeRates(TABLE_TYPE);

        if (!exchangeRateDTO.isEmpty()) {
            log.info("Save actual Rates");
            ratesRegistry.saveAll(exchangeRateDTO.getFirst().getRates());
            log.info("Add PLN Rate");
            RateDTO PLN = RateDTO.builder()
                    .ask(BigDecimal.valueOf(1L))
                    .currency("złoty polski")
                    .bid(BigDecimal.valueOf(1L))
                    .code("PLN")
                    .build();
            ratesRegistry.save("PLN", PLN);
            log.info("Successful Save actual Rates");
        }
    }
}
