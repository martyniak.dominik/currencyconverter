package pl.martyniak.dominik.currencyconverter.models.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
public class ConvertResponseDTO {
    @ApiModelProperty(notes = "Conversion value")
    BigDecimal amount;
    @ApiModelProperty(notes = "Input currency symbol")
    String inputCurrency;
    @ApiModelProperty(notes = "Output currency symbol")
    String outputCurrency;
    @ApiModelProperty(notes = "results of conversion")
    BigDecimal convertedAmount;
}
