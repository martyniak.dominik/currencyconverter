package pl.martyniak.dominik.currencyconverter.models.dto;

import lombok.*;

/**
 * Custom exception body response
 *
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExceptionBody {
    Integer status;
    String message;

}
