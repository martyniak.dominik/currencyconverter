package pl.martyniak.dominik.currencyconverter.models.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Exchange Rate Dto, response from NBP API
 *
 */
@Getter
@Setter
@Builder
public class ExchangeRateDTO {
    private String table;
    private String no;
    private String effectiveDate;
    private List<RateDTO> rates;
}
