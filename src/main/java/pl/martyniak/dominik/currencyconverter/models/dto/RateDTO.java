package pl.martyniak.dominik.currencyconverter.models.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Single Rate Dto, response from NBP API
 *
 */
@Getter
@Setter
@Builder
public class RateDTO {
    private String currency;
    private String code;
    private BigDecimal bid;
    private BigDecimal ask;
}
