package pl.martyniak.dominik.currencyconverter.utils;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.martyniak.dominik.currencyconverter.models.dto.ExceptionBody;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@Log4j2
public class ExceptionHandlingController {

    @ExceptionHandler(IncorrectAmountException.class)
    @ResponseBody
    public ResponseEntity<ExceptionBody> handleErrorIncorrectAmountException(HttpServletRequest req, Exception ex) {

        return new ResponseEntity<ExceptionBody>(
                ExceptionBody.builder().message(ex.getMessage()).status(422).build(),
                HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(NotFoundCurrencyCodeException.class)
    @ResponseBody
    public ResponseEntity<ExceptionBody> handleErrorNotFoundCurrencyCodeException(HttpServletRequest req, Exception ex) {

        return new ResponseEntity<ExceptionBody>(
                ExceptionBody.builder().message(ex.getMessage()).status(422).build(),
                HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
