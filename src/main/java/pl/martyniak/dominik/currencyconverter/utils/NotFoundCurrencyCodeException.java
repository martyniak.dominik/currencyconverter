package pl.martyniak.dominik.currencyconverter.utils;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
public class NotFoundCurrencyCodeException extends RuntimeException {

    /**
     * Creates the exception with the error message and error code received from external system.
     *
     * @param message error message
     */
    public NotFoundCurrencyCodeException(String message) {
        super(message);
    }

}
