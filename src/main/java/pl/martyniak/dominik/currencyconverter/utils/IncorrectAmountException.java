package pl.martyniak.dominik.currencyconverter.utils;


public class IncorrectAmountException extends RuntimeException {

    /**
     * Creates the exception with the error message and error code received from IncorrectAmountException.
     *
     * @param message error message
     */
    public IncorrectAmountException(String message) {
        super(message);
    }

}
