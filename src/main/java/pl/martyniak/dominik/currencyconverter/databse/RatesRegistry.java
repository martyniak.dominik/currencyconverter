package pl.martyniak.dominik.currencyconverter.databse;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.martyniak.dominik.currencyconverter.models.dto.ExchangeRateDTO;
import pl.martyniak.dominik.currencyconverter.models.dto.RateDTO;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Registry to keep RateDTO
 *
 */

@RequiredArgsConstructor
@Component
public class RatesRegistry {

    private final ConcurrentHashMap<String, RateDTO> registry = new ConcurrentHashMap<>();


    /**
     * Method save single RateDTO {@link ExchangeRateDTO}
     *
     * @param code code of currency
     * @param rateDTO {@link ExchangeRateDTO}
     */
    public void save(String code, RateDTO rateDTO) {
        registry.put(code, rateDTO);
    }

    /**
     * Method save list of RateDTO {@link ExchangeRateDTO}
     *
     * @param rateList List of {@link ExchangeRateDTO}
     */
    public void saveAll(List<RateDTO> rateList) {
        rateList.forEach( rate -> registry.put(rate.getCode(), rate));
    }

    /**
     * Method to get single  RateDTO {@link ExchangeRateDTO} by code of currency
     * When not found, method will return Null
     * @param code code of currency
     * @return {@link ExchangeRateDTO}
     */
    public RateDTO getByCode( String code){
        return registry.get(code);
    }

    /**
     * Method to get all of  RateDTO {@link ExchangeRateDTO}
     * When not found, method will return Null
     * @return Collection of {@link ExchangeRateDTO}
     */

    public Collection<RateDTO> getAll(){
        return registry.values();
    }
}
