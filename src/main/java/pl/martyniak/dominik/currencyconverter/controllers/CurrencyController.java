package pl.martyniak.dominik.currencyconverter.controllers;


import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.martyniak.dominik.currencyconverter.models.dto.ConvertResponseDTO;
import pl.martyniak.dominik.currencyconverter.services.CurrencyService;

import java.math.BigDecimal;
import java.util.List;


@RestController
@RequestMapping("${spring.application.base-url-path}/currency")
@RequiredArgsConstructor
public class CurrencyController {

    private final CurrencyService currencyService;


    @GetMapping("convert")
    public ConvertResponseDTO convertCurrent(
            @ApiParam(name = "amount", value = "The amount to be converted", required = true)
            @RequestParam("amount") BigDecimal amount,
            @ApiParam(name = "input-currency", value = "Currency CODE of the amount", required = true)
            @RequestParam("input-currency") String inputCurrency,
            @ApiParam(name = "output-currency", value = "Target currency CODE", required = true)
            @RequestParam("output-currency") String outputCurrency) {
        return currencyService.convertAmount(amount,inputCurrency,outputCurrency);

    }

    @GetMapping("available-currency-code")
    public List<String> getAvailableCurrencyCode() {
        return currencyService.getAvailableCurrencyCode();
    }

}
