package pl.martyniak.dominik.currencyconverter.services;




import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.junit.jupiter.api.Test;

import pl.martyniak.dominik.currencyconverter.databse.RatesRegistry;
import pl.martyniak.dominik.currencyconverter.models.dto.ConvertResponseDTO;
import pl.martyniak.dominik.currencyconverter.models.dto.RateDTO;
import pl.martyniak.dominik.currencyconverter.utils.NotFoundCurrencyCodeException;

import java.math.BigDecimal;

import static org.assertj.core.api.BDDAssertions.then;
import static org.assertj.core.api.BDDAssertions.thenThrownBy;
import static org.mockito.Mockito.when;

@SpringBootTest
public class CurrencyServiceTest {

    @MockBean
    private RatesRegistry ratesRegistry;

    private CurrencyService toTest;

    @BeforeEach
    public void setUp() {
        toTest = new CurrencyService(ratesRegistry);
    }

    @Test
    public void should_throw_exc_on_for_non_existing_currency_code() {
        // given
        String CODE_INPUT = "ABCDF";
        String CODE_OUTPUT = "12345GGG";
        BigDecimal AMOUNT = BigDecimal.valueOf(13L);
        // when
        when(ratesRegistry.getByCode(CODE_INPUT)).thenReturn(null);
        // then
        thenThrownBy(() -> toTest.convertAmount(AMOUNT,CODE_INPUT,CODE_OUTPUT))
                .isInstanceOf(NotFoundCurrencyCodeException.class);
    }

    @Test
    public void should_return_correct_value() {
        // given
        String CODE_INPUT = "CODE_1";
        String CODE_OUTPUT = "CODE_2";
        BigDecimal AMOUNT = BigDecimal.valueOf(13L);
        // when
        when(ratesRegistry.getByCode(CODE_INPUT)).thenReturn(
                RateDTO.builder()
                        .ask(BigDecimal.valueOf(1))
                        .bid(BigDecimal.valueOf(1))
                        .build()
        );
        when(ratesRegistry.getByCode(CODE_OUTPUT)).thenReturn(
                RateDTO.builder()
                        .ask(BigDecimal.valueOf(2))
                        .bid(BigDecimal.valueOf(2))
                        .build()
        );
        // then
        ConvertResponseDTO result = toTest.convertAmount(AMOUNT,CODE_INPUT,CODE_OUTPUT);

        then(result.getAmount()).isEqualTo(AMOUNT);
        then(result.getConvertedAmount()).isEqualTo(BigDecimal.valueOf(6.5));
        then(result.getInputCurrency()).isEqualTo(CODE_INPUT);
        then(result.getOutputCurrency()).isEqualTo(CODE_OUTPUT);
    }

    @Test
    public void should_throw_exc_on_negative_value() {
        // given
        String CODE_INPUT = "CODE_1";
        String CODE_OUTPUT = "CODE_2";
        BigDecimal AMOUNT = BigDecimal.valueOf(-13L);
        // when
        when(ratesRegistry.getByCode(CODE_INPUT)).thenReturn(
                RateDTO.builder()
                        .ask(BigDecimal.valueOf(1))
                        .bid(BigDecimal.valueOf(1))
                        .build()
        );
        when(ratesRegistry.getByCode(CODE_OUTPUT)).thenReturn(
                RateDTO.builder()
                        .ask(BigDecimal.valueOf(2))
                        .bid(BigDecimal.valueOf(2))
                        .build()
        );
        // then
        // when
        when(ratesRegistry.getByCode(CODE_INPUT)).thenReturn(null);
        // then
        thenThrownBy(() -> toTest.convertAmount(AMOUNT,CODE_INPUT,CODE_OUTPUT))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    public void should_throw_exc_on_0_value() {
        // given
        String CODE_INPUT = "CODE_1";
        String CODE_OUTPUT = "CODE_2";
        BigDecimal AMOUNT = BigDecimal.valueOf(0L);
        // when
        when(ratesRegistry.getByCode(CODE_INPUT)).thenReturn(
                RateDTO.builder()
                        .ask(BigDecimal.valueOf(1))
                        .bid(BigDecimal.valueOf(1))
                        .build()
        );
        when(ratesRegistry.getByCode(CODE_OUTPUT)).thenReturn(
                RateDTO.builder()
                        .ask(BigDecimal.valueOf(2))
                        .bid(BigDecimal.valueOf(2))
                        .build()
        );
        // then
        // when
        when(ratesRegistry.getByCode(CODE_INPUT)).thenReturn(null);
        // then
        thenThrownBy(() -> toTest.convertAmount(AMOUNT,CODE_INPUT,CODE_OUTPUT))
                .isInstanceOf(RuntimeException.class);
    }

}
