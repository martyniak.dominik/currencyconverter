# CurrencyConverter
## Requirements
* JDK8
* Gradle 6.3
* Docker 

## Running app 
### with Docker
1. gradlew build
2. Build ```docker build -t converter .```
3. Run image ```docker run -p 8080:8080 converter```

### with java
1. gradlew build
2. java -jar ./build/libs/currency-converter-0.0.1-SNAPSHOT.jar

### from IntelliJ
1. Run ``CurrencyConverterApplication`` app directly from IntelliJ.

## Swagger endpoints
* http://localhost:8080/swagger-ui.html
